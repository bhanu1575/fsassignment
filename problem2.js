/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs')


function readFile(fileName){
    return fs.promises.readFile(fileName,'utf-8')
}

function writeFile(fileName,data){
    return fs.promises.writeFile(fileName,data).then((res)=>{
        if(res === undefined){
            return `${fileName} created Successfully`
        }
        return res
    })
}
function appendFile(fileName,data){
    return fs.promises.appendFile(fileName,data).then((res)=>{
        if(res === undefined){
            return `${fileName} appended with ${data} Successfully`
        }
        return res
    })
}
function deleteFile(fileName){
    return fs.promises.unlink(fileName).then((res)=>{
        if(res === undefined){
            return `${fileName} deleted Successfully`
        }
        return res
    })
}

module.exports = {readFile,writeFile,deleteFile,appendFile}