const {readFile, deleteFile, writeFile, appendFile} = require('../problem2')

function readAndWrite(){
    
readFile('./lipsum.txt')
.then((response)=>{
    return writeFile('./uppertext.txt',response.toUpperCase())
})
.then((response)=>{
    console.log(response);
    return writeFile('./filenames.txt','')
})
.then((response)=>{
    console.log(response);
    return appendFile('./filenames.txt','uppertext.txt,')
})
.then((response)=>{
    console.log(response);
    return readFile('./uppertext.txt')
})
.then((response)=>{
    return writeFile('./splittedtext.txt',JSON.stringify(response.split('. ')))
})
.then((response)=>{
    console.log(response);
    return appendFile('./filenames.txt','splittedtext.txt,')
})
.then((response)=>{
    console.log(response);
    return readFile('./splittedtext.txt')
})
.then((response)=>{
    let sortedData = (JSON.parse(response).sort())
    return writeFile('./sortedtext.txt',JSON.stringify(sortedData))
})
.then((response)=>{
    console.log(response);
    return appendFile('./filenames.txt','sortedtext.txt,')
})
.then((response)=>{
    console.log(response);
    return readFile('./filenames.txt')
})
.then((response)=>{
    let filenames = response.split(',').slice(0,-1)
    console.log('files created successfully',filenames);
    let deleteAllFiles = Promise.all(filenames.map(fileName => deleteFile(fileName)))
    return deleteAllFiles
})
.then((response)=>{
    console.log(response);
})
.catch((err)=>{
    console.log(err.message);
})
}


try{
    readAndWrite()
} catch(error) {
    console.log(error.message)
}