const { createDirectory, createFile, deleteFile } = require("../problem1");

let dirName = "public";
let count = 5;
function createAllFiles(noOfFiles) {
  let promises = [];
  for (let fileCount = 0; fileCount < noOfFiles; fileCount++) {
    promises.push(createFile(`./${dirName}/file-${fileCount}.json`));
  }

  return Promise.all(promises);
}
function deleteAllFiles(noOfFiles) {
  let promises = [];
  for (let fileCount = 0; fileCount < noOfFiles; fileCount++) {
    promises.push(deleteFile(`./${dirName}/file-${fileCount}.json`));
  }
  return Promise.all(promises);
}
function createAndDelete() {
  createDirectory(dirName)
    .then((res) => {
      console.log(res);
      return createAllFiles(count);
    })
    .then((res) => {
      console.log(res);
      return deleteAllFiles(count);
    })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}

try {
  createAndDelete()
} catch (err) {
  console.log(err.message);
}
