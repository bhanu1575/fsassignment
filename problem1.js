/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs')

function createDirectory(name){
    if(!name){
        throw new Error('Directory name cannot be an empty value')
    }
    return fs.promises.mkdir(name).then((res)=>{
        if(res === undefined){
            return 'Directory Created Successful'
        }
        return res
    })
}

function createFile(filePath){
    return fs.promises.writeFile(filePath,'Hello').then((res)=>{
        if(res === undefined){
            return `${filePath} created Successful`
        }
        return res
    })

}
function deleteFile(filePath){
    return fs.promises.unlink(filePath).then((res)=>{
        if(res === undefined){
            return `${filePath} deleted Successful`
        }
        return res
    })
}

module.exports = {createDirectory,createFile,deleteFile}